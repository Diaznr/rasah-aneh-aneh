#!/bin/bash

NUM_CPU_CORES=2

cpulimit -e "bionic" -l $((80 * $NUM_CPU_CORES))
